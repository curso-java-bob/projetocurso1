package cursojava.executavel;

public class Aula7L5 {

	/* Variavel Global e acessivel a todos */
	static int maiorIdadeGlobal = 30;

	/* Main e um metodo auto executavel em Java */
	public static void main(String[] args) {

		/* Variavel Local porque pertence somente a esse metodo e o valor fica dentro do metodo */
		int maiorIdade = 18;
		System.out.println("Valor da Variavel Local = " + maiorIdade);
		System.out.println("Valor da Variavel Global = " + maiorIdadeGlobal);

		metodo2();

	}

	public static void metodo2() {
	int mediaAno = 50;
		System.out.println("Valor da Variavel Global = " +  maiorIdadeGlobal);
	}
}
