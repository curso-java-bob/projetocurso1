package cursojava.executavel;

public class Aninhadas {

	/* Main e um metodo auto executavel em Java */
	public static void main(String[] args) {

		int nota1 = 95;
		int nota2 = 90;
		int nota3 = 90;
		int nota4 = 95;
		int media = 0;

		media = (nota1 + nota2 + nota3 + nota4) / 4;

		/* Operacoes logicas aninhadas: Sao operacoes dentro de operacoes */

		if (media > 50) {
			if (media >= 70) {
				if (media > 90) {
					System.out.println("Aluno aprovado, parabens : " + media);
				} else {
					System.out.println("Aluno aprovado : " + media);
				}
			} else {
				System.out.println("Aluno reporvado : " + media);
			}
		}

	}
}