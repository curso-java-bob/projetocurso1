package cursojava.executavel;

import javax.swing.JOptionPane;

public class Criando_opcao_de_confirmacao_Importante {

	/* Main e um metodo auto executavel em Java */
	public static void main(String[] args) {

		/* VARIAVEL RECEBE JOPTIONPANE */
		String carros = JOptionPane.showInputDialog("Informe a quantidade de carros? ");
		String pessoas = JOptionPane.showInputDialog("Informe a quantidade de pessoas? ");

		double carroNumero = Double.parseDouble(carros);
		double pessoasNumero = Double.parseDouble(pessoas);

		int divisao = (int) (carroNumero / pessoasNumero);

		double resto = carroNumero % pessoasNumero;

		int resposta = JOptionPane.showConfirmDialog(null, " Deseja ver o resultado da divisao ? ");

		if (resposta == 0) {

			JOptionPane.showMessageDialog(null, " Divisao para pessoas deu " + divisao);
		}

		resposta = JOptionPane.showConfirmDialog(null, " Deseja ver o resto da divisao ? ");

		if (resposta == 0) {

			JOptionPane.showMessageDialog(null, " O resto da divisao e : " + resto);

		}

	}
}