package cursojava.executavel;

public class SWITCH {

	/* Main � um metodo auto executavel em Java */
	public static void main(String[] args) {

		int nota1 = 95;
		int nota2 = 90;
		int nota3 = 90;
		int nota4 = 95;
		int media = 0;

		media = (nota1 + nota2 + nota3 + nota4) / 4;

		/* SWITCH CASE: OPERA��ES EXATAS */
		
		int dia = 2;
		switch (dia) {
		case 1:
			System.out.println("� domingo");
			break;
		case 2:
			System.out.println("� segunda");
			break;

		default: System.out.println("Outro dia qualquer");
			break;
		}
		

	}
}