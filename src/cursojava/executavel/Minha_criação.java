package cursojava.executavel;

import javax.swing.JOptionPane;

 class Minha_criacao {

	/* Main e um metodo auto executavel em Java */
	public static void main(String[] args) {

		/* VARIAVEL RECEBE JOPTIONPANE */
		String nota1 = JOptionPane.showInputDialog("Informe a primeira nota ");
		String nota2 = JOptionPane.showInputDialog("Informe a segunda nota ");
		String nota3 = JOptionPane.showInputDialog("Informe a terceira nota ");
		String nota4 = JOptionPane.showInputDialog("Informe a quarta nota ");

		double nota1Valor = Double.parseDouble(nota1);
		double nota2Valor = Double.parseDouble(nota2);
		double nota3Valor = Double.parseDouble(nota3);
		double nota4Valor = Double.parseDouble(nota4);

		String recuperacao = "Aluno em recuperacao";
		String reprovado = "Aluno reprovado estude mais";
		String aprovado = "Aluno aprovado";

		double media = (nota1Valor + nota2Valor + nota3Valor + nota4Valor) / 4;

		int pergunta = JOptionPane.showConfirmDialog(null, " Deseja ver o resultado da media ? ");

		if (pergunta == 0) {

			JOptionPane.showMessageDialog(null, " Media do aluno foi : " + media);

		}

		pergunta = JOptionPane.showConfirmDialog(null, " Deseja ver se ele foi aprovado ? ");

		if (pergunta == 0) {

			if (media >= 70) {

				JOptionPane.showMessageDialog(null, aprovado);

			} else if (media >= 50 && media <= 69) {

				JOptionPane.showMessageDialog(null, recuperacao);

			} else if (media < 50) {

				JOptionPane.showMessageDialog(null, reprovado);
			}

		}

	}

}