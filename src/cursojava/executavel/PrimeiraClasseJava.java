package cursojava.executavel;

import javax.swing.JOptionPane;

public class PrimeiraClasseJava {

	/* Main � um metodo auto executavel em Java */
	public static void main(String[] args) {

		/* VARIAVEL RECEBE JOPTIONPANE */
		String nota1 = JOptionPane.showInputDialog("Informe a primeira nota ");
		String nota2 = JOptionPane.showInputDialog("Informe a segunda nota ");
		String nota3 = JOptionPane.showInputDialog("Informe a terceira nota ");
		String nota4 = JOptionPane.showInputDialog("Informe a quarta nota ");

		double nota1Valor = Double.parseDouble(nota1);
		double nota2Valor = Double.parseDouble(nota2);
		double nota3Valor = Double.parseDouble(nota3);
		double nota4Valor = Double.parseDouble(nota4);

		String recuperacao = "Aluno em recupera��o";
		String reprovado = "Aluno reprovado";
		String aprovado = "Aluno aprovado";

		double media = (nota1Valor + nota2Valor + nota3Valor + nota4Valor) / 4;

		int pergunta = JOptionPane.showConfirmDialog(null, " Deseja ver se ele foi aprovado ? ");

		if (pergunta == 0) {

			if (media >= 70) {

				JOptionPane.showMessageDialog(null, aprovado + " com m�dia de : " + media);

			} else if (media >= 50) {

				JOptionPane.showMessageDialog(null, recuperacao + " com m�dia de : " + media);

			} else {

				JOptionPane.showMessageDialog(null, reprovado + " com m�dia de : " + media + " estude mais");
			}

		}

	}

}